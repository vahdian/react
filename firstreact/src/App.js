
import './App.css';
import { HelloWorld } from './components/HelloWorld';
import { HelloVar } from './components/HelloVar';
import { HelloUser } from './components/HelloUser';
import { Card } from './components/Card';
import { HelloProps } from './components/HelloProps';
import { ButtonProps } from './components/ButtonProps';
import { ButtonAlert } from './components/ButtonAlert';
import { Counter } from './components/Counter';
 



function App() {
  return (
    <div className="App">
      <header className="App-header">
       <HelloWorld></HelloWorld>
       <HelloVar></HelloVar>
       <HelloUser></HelloUser>
       <Card></Card>
       <HelloProps name='Legolas del  musguillo' age={34}></HelloProps>
       <ButtonProps text='botonaco'></ButtonProps>
       <ButtonAlert></ButtonAlert>
       <Counter></Counter>
       </header>
    </div>
  );
}

export default App;
