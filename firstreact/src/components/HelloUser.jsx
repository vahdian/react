import React from 'react';

export class HelloUser extends React.Component{
    render(){
        const user = {name:'Yoda', age:875}
        
        return(
           
            <h1>Hello {user.name} you are {user.age}</h1>
        )
    }
}