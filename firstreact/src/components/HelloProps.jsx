import React from 'react';

export class HelloProps extends React.Component{
    render(){
        return(
            <h1> Hello {this.props.name}, you are {this.props.age} years old.</h1>
        )
    }
}