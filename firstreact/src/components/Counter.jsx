import React from 'react';

export class Counter extends React.Component {
    state = { count: 0};

    add = () => {
        this.setState({ count: this.state.count +1})
        /* this.backUpCount++;
        console.log(this.backUpCount) */
    }
    substract = () => {
        this.setState({ count: this.state.count -1})
        /* this.backUpCount++;
        console.log(this.backUpCount) */
    }
    multiply = () => {
        this.setState({ count: this.state.count * 2})
        /* this.backUpCount++;
        console.log(this.backUpCount) */
    }
    divide = () => {
        this.setState({ count: this.state.count /2})
        /* this.backUpCount++;
        console.log(this.backUpCount) */
    }
    reset = () => {
        this.setState({ count: 0})
        /* this.backUpCount++;
        console.log(this.backUpCount) */
    }



render(){
    return(
        <div>
            <h2>The count is {this.state.count}</h2>
            <button onClick={this.add}>+1</button>
            <button onClick={this.substract}>-1</button>
            <button onClick={this.multiply}>x2</button>
            <button onClick={this.divide}>/2</button>
            <button onClick={this.reset}>RESET</button>
        </div>
    )
}   
}