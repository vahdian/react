import React from 'react';

export class HelloVar extends React.Component{
    render(){
        const name = 'Abel Cabeza Roman';
        return(
           
            <h1>Hello {name}</h1>
        )
    }
}