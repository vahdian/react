import './App.css';
import { HelloProps } from './components/HelloProps/HelloProps';
import { ListProps } from './components/ListProps/ListProps';
import { UserList} from './components/UserList/UserList';
import { Counter } from './components/Counter/Counter';
import {Gallery} from './components/Gallery/Gallery'


function App() {
  return (
    <div className="App">
    <header className ="App-header">
    <HelloProps name="German" age={97}></HelloProps>
    <ListProps list={['Walk the dogs', 'Shower Mr.Whiskers', 'Hide the bodies']}></ListProps>
    <UserList userList={[{name: 'Abel Cabeza Roman', role:'profe', age:25}, {name: 'Pinocho', role:'Invasor de cienagas', age:19}, {name: 'Chundasvinto', role:'Rey Godo', age:1876}]}></UserList>
    <Counter></Counter>
    <Gallery galleryList={[
      {title: 'Paisaje tripresioso', imgUrl:'https://www.usnews.com/dims4/USNEWS/f39e4a7/2147483647/thumbnail/640x420/quality/85/?url=http%3A%2F%2Fmedia.beam.usnews.com%2Fff%2F6d%2Fabb5206e42b4932cf58355abad4b%2F1-intro-iguazu-falls.jpg'},
      {title: 'Beyonsi', imgUrl:'https://e00-elmundo.uecdn.es/assets/multimedia/imagenes/2019/01/28/15486877845436.jpg'},
      {title: 'Paisaje tripresioso', imgUrl:'https://sm.mashable.com/mashable_in/seo/6/6512/6512_x68k.jpg'}
    ]}/>
    </header>
    </div>
  );
}

export default App;
