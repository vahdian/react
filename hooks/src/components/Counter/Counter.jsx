import React, {useState} from 'react';

export function Counter(){
    const [count, setCount] = useState(0);
    return(
        <div>
            <p>You clicked {count} times</p>
            <button onClick={()=> setCount(count +1)}>
               Add
            </button>
            <button onClick={()=> setCount(count -1)}>
                Substract
            </button>
            <button onClick={()=> setCount(0)}>
                Reset
            </button>
        </div>
    )
}