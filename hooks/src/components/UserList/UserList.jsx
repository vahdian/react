import React, { useState } from "react";
import { Button } from "../Button/Button";

export function UserList(props) {
  const[userList, setUserList] = useState(props.userList);

  const removeUser = (index) => {
    const localUserList = [ ...userList ];
    localUserList.splice(index,1);
    setUserList(localUserList);
  };
  return (
    <ul>
      {userList.map((user, i) => 
        <li key={i}>
          My name is {user.name}, I am a {user.role} and I am currently
          {user.age}
          <Button index={i} fnClickedButton={removeUser} text="X"/>
        </li>
      )}
    </ul>
  );
}
