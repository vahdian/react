import React, {useState} from 'react';
import {GalleryList} from './components/GalleryList/GalleryList';

export function Gallery(props){
    const [galleryList, setGalleryList] = useState(props.galleryList)
    return(
    <div>
        <GalleryList galleryList={galleryList}></GalleryList>
    </div>
    )
}