import React, { useState} from 'react';

export function GalleryList(props){
    return(
        <div>
            {props.galleryList.map((img,i)=>
            <figure key={i}>
                <figcaption>{img.title}</figcaption>
                <img src={img.imgUrl} alt=""/>
            </figure>)}
        </div>
    )
}