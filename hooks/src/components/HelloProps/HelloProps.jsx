import React from 'react';

export function HelloProps(props){
    return(
        <h1>Hello {props.name}, you are {props.age} years old</h1>
    )
}

/* export function HelloProps({name, age}){
    return(
        <h1>Hello{props.name}, you are {props.age} years old</h1>
    )
} */