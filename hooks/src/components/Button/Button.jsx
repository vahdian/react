import React from 'react';

export function Button(props){
    return(
        <button onClick={()=>props.fnClickedButton(props.index)}>{props.text}</button>
    )
}